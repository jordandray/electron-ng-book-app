import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookViewerComponent } from './components/book-viewer/book-viewer.component';

const routes: Routes = [
  { path: '', component: BookViewerComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookViewerRoutingModule { }
