import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromRoot from '../../state/app.state';
import * as fromBookViewer from './reducers/book-viewer.reducer';

export interface State extends fromRoot.State {
  bookViewer: fromBookViewer.BookViewerState;
}

const getBookViewerFeatureState = createFeatureSelector<fromBookViewer.BookViewerState>('bookViewer');

export const getBookList = createSelector(
  getBookViewerFeatureState,
  state => {
    return state.bookList;
  }
);

export const getLoadingBookList = createSelector(
  getBookViewerFeatureState,
  state => state.isLoadingBookList
);

export const getSelectedBook = createSelector(
  getBookViewerFeatureState,
  state => state.selectedBook
);

export const getLoadingBook = createSelector(
  getBookViewerFeatureState,
  state => {
    return state.isLoadingBook;
  }
);
