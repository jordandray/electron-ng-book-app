import { Book } from '../../models/book';
import { BookViewerActions, BookViewerActionTypes } from '../actions/book-viewer.actions';

export interface BookViewerState {
  selectedBook?: Book;
  isLoadingBook: boolean;
  isOnline: boolean;
  errorMessage: string;
  bookList: Book[];
  isLoadingBookList: boolean;
}

const initialState: BookViewerState = {
  selectedBook: null,
  isLoadingBook: false,
  isOnline: true,
  errorMessage: '',
  bookList: [],
  isLoadingBookList: false
};

export function reducer(state = initialState, action: BookViewerActions): BookViewerState {
  switch (action.type) {
    case BookViewerActionTypes.TryLoadBookList:
      return {
        ...state,
        isLoadingBookList: true
      };

    case BookViewerActionTypes.LoadBookListSuccess:
      return {
        ...state,
        isOnline: true,
        bookList: action.payload,
        isLoadingBookList: false
      };

    case BookViewerActionTypes.LoadBookListFailure:
      return {
        ...state,
        isOnline: false,
        errorMessage: 'Unable to retrieve list of Books.',
        isLoadingBookList: false
      };

    case BookViewerActionTypes.TryLoadBook:
      return {
        ...state,
        isLoadingBook: true
      };

    case BookViewerActionTypes.LoadBookSuccess:
      return {
        ...state,
        selectedBook: action.payload,
        isLoadingBook: false
      };

    case BookViewerActionTypes.LoadBookFailure:
      return {
        ...state,
        isOnline: false,
        isLoadingBook: false,
        errorMessage: 'Unable to retrieve book.'
      };

    default:
      return state;
  }
}
