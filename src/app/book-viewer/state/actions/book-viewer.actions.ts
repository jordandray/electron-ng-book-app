import { Action } from '@ngrx/store';
import { Book } from '../../models/book';

export enum BookViewerActionTypes {
  TryLoadBookList = '[Book Viewer] Try to Load Book list',
  LoadBookListSuccess = '[Book Viewer] Load Book list success',
  LoadBookListFailure = '[Book Viewer] Load Book list failure',
  TryLoadBook = '[Book Viewer] Try to load book',
  LoadBookSuccess = '[Book Viewer] Load book success',
  LoadBookFailure = '[Book Viewer] Load book failure'
}

export class TryLoadBookList implements Action {
  readonly type = BookViewerActionTypes.TryLoadBookList;

  constructor() { }
}

export class LoadBookListSuccess implements Action {
  readonly type = BookViewerActionTypes.LoadBookListSuccess;

  constructor(public payload: Book[]) { }
}

export class LoadBookListFailure implements Action {
  readonly type = BookViewerActionTypes.LoadBookListFailure;

  constructor(public payload: string) { }
}

export class TryLoadBook implements Action {
  readonly type = BookViewerActionTypes.TryLoadBook;

  constructor(public payload: number) { }
}

export class LoadBookSuccess implements Action {
  readonly type = BookViewerActionTypes.LoadBookSuccess;

  constructor(public payload: Book) { }
}

export class LoadBookFailure implements Action {
  readonly type = BookViewerActionTypes.LoadBookFailure;

  constructor(public payload: string) { }
}

export type BookViewerActions = TryLoadBookList
| LoadBookListSuccess
| LoadBookListFailure
| TryLoadBook
| LoadBookSuccess
| LoadBookFailure;
