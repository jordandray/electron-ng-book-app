import { Injectable } from "@angular/core";
import { BookService } from '../../services/book.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import * as bookViewerActions from '../actions/book-viewer.actions';
import { catchError, mergeMap, map } from 'rxjs/operators';

@Injectable()
export class BookViewerEffects {

  constructor(
    private _bookService: BookService,
    private _actions$: Actions
  ) { }

  @Effect()
  loadBookList$: Observable<Action> = this._actions$.pipe(
    ofType(bookViewerActions.BookViewerActionTypes.TryLoadBookList),
    mergeMap(action => {
      return this._bookService.getBookList().pipe(
        map(books => (new bookViewerActions.LoadBookListSuccess(books))),
        catchError(err => of(new bookViewerActions.LoadBookListFailure(err))))
      })
  );

  @Effect()
  loadBook$: Observable<Action> = this._actions$.pipe(
    ofType(bookViewerActions.BookViewerActionTypes.TryLoadBook),
    map((action: bookViewerActions.TryLoadBook) => action.payload),
    mergeMap(bookId => {
      return this._bookService.getBookById(bookId).pipe(
        map(book => (new bookViewerActions.LoadBookSuccess(book))),
        catchError(err => of(new bookViewerActions.LoadBookFailure(err))))
    })
  );
}
