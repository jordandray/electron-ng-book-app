import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Book } from '../models/book';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private _bookDb: Book[];

  constructor() {
    this._bookDb = [
      {
        id: 0,
        title: 'Soar and Beat',
        genre: 'Philosophy',
        isbn: 'e845ti-w23d5-idont-knoww-hatth-eselo-oklike',
        authors: [
          {
            id: 0,
            firstName: 'Ivan',
            middleInitial: 'B',
            lastName: 'Madeup'
          }
        ],
        publisher: {
          id: 0,
          name: 'Arctic Bird Publishing',
          contactPhone: '555-555-5555',
          contactName: 'Mr. Flightless'
        },
        publishDate: 'April 1, 1980',
        pages: 689
      },
      {
        id: 1,
        title: 'Google Fu',
        genre: 'Technology',
        isbn: '555-55555-555555-555555-55555-55555',
        authors: [
          {
            id: 1,
            firstName: 'Ricky',
            middleInitial: 'S',
            lastName: 'Martin'
          }
        ],
        publisher: {
          id: 0,
          name: 'Arctic Bird Publishing',
          contactPhone: '555-555-5555',
          contactName: 'Mr. Flightless'
        },
        publishDate: 'December 15, 2003',
        pages: 234
      },
      {
        id: 2,
        title: 'All About Arctic Birds and Publishing',
        genre: 'Business',
        isbn: '777777-777777-77777-77777-888888-888888',
        authors: [
          {
            id: 2,
            firstName: 'Bob',
            lastName: 'Bird'
          },
          {
            id: 3,
            firstName: 'Sally',
            lastName: 'Sandals'
          }
        ],
        publisher: {
          id: 1,
          name: 'Rival Publishing',
          contactPhone: '555-222-2222',
          contactName: 'Rich Sandals'
        },
        publishDate: 'June 5, 2010',
        pages: 452
      },
      {
        id: 3,
        title: 'Stock Market: Making a Business',
        genre: 'Business',
        isbn: '777777-777777-77777-77777-888888-888888',
        authors: [
          {
            id: 4,
            firstName: 'Vincent',
            lastName: 'Adultman'
          }
        ],
        publisher: {
          id: 1,
          name: 'Rival Publishing',
          contactPhone: '555-222-2222',
          contactName: 'Rich Sandals'
        },
        publishDate: 'May 10, 2017',
        pages: 92
      }
    ];
  }

  public getBookList(): Observable<Book[]> {
    return of(this._bookDb.map(book => {
      return {
        ...book,
        publisher: {
          ...book.publisher,
          contactName: null,
          contactPhone: null
        }
      }
    })).pipe(delay(2000));
  }

  public getBookById(id: number): Observable<Book> {
      return of(this._bookDb.find(b => b.id === id)).pipe(delay(2000));
  }
}
