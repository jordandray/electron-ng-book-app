import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import * as fromBookViewer from '../../state';
import * as bookViewerActions from '../../state/actions/book-viewer.actions';
import { Observable, Subscription } from 'rxjs';
import { Book } from '../../models/book';
import { MatDialog } from '@angular/material';
import { BookDialogComponent } from '../book-dialog/book-dialog.component';
import { GridApi } from 'ag-grid-community';

@Component({
  selector: 'app-book-viewer',
  templateUrl: './book-viewer.component.html',
  styleUrls: ['./book-viewer.component.css']
})
export class BookViewerComponent implements OnInit, OnDestroy {
  private _gridApi: GridApi;

  private _columnDefs = [
    { headerName: 'Title', field: 'title' },
    { headerName: 'Genre', field: 'genre'},
    { headerName: 'Publisher', field: 'publisher.name' },
    { headerName: 'Pages', field: 'pages' }
  ];

  private _isLoadingbookListSubscription: Subscription;

  private _bookList$: Observable<Book[]>;

  get columnDefs() {
    return this._columnDefs;
  }

  get bookList$() {
    return this._bookList$;
  }

  constructor(
    private _store: Store<fromBookViewer.State>,
    private _dialog: MatDialog) {}

  ngOnInit() {
    this._store.dispatch(new bookViewerActions.TryLoadBookList());

    this._bookList$ = this._store.pipe(
      select(fromBookViewer.getBookList));
  }

  ngOnDestroy() {
    this._isLoadingbookListSubscription.unsubscribe();
  }

  public rowClicked(event) {
    const selectedBook = event.data as Book;
    this._dialog.open(
      BookDialogComponent,
      {
        width: '60%',
        data: selectedBook.id
      }
    );
  }

  public onGridReady(grid) {
    this._gridApi = grid.api;

    this._gridApi.sizeColumnsToFit();
    this._isLoadingbookListSubscription = this._store.pipe(
      select(fromBookViewer.getLoadingBookList)).subscribe(loading => {
        loading ?
          this._gridApi.showLoadingOverlay() :
          this._gridApi.hideOverlay();
      });
  }

  public onGridResizeChanged(grid) {
    if (this._gridApi) {
      this._gridApi.sizeColumnsToFit();
    }
  }
}
