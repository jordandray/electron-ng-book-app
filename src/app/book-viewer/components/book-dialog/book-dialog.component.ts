import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable, Subscription } from 'rxjs';
import { Book } from '../../models/book';

import * as fromBookViewer from '../../state';
import * as bookViewerActions from '../../state/actions/book-viewer.actions';
import { Store, select } from '@ngrx/store';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-book-dialog',
  templateUrl: './book-dialog.component.html',
  styleUrls: ['./book-dialog.component.css']
})
export class BookDialogComponent implements OnInit, OnDestroy {
  private _bookSubscription: Subscription;
  private _bookFormGroup: FormGroup;
  private _isLoadingBook$: Observable<boolean>;

  get bookFormGroup() {
    return this._bookFormGroup;
  }

  get publisherFormGroup() {
    return this._bookFormGroup.get('publisher');
  }

  get authorsFormArray(): FormArray {
    return this._bookFormGroup.get('authors') as FormArray;
  }

  get isLoadingBook$() {
    return this._isLoadingBook$;
  }

  constructor(
    private _dialogRef: MatDialogRef<BookDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _bookId: number,
    private _store: Store<fromBookViewer.State>,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this._store.dispatch(new bookViewerActions.TryLoadBook(this._bookId));

    this._isLoadingBook$ = this._store.pipe(select(fromBookViewer.getLoadingBook));

    this._bookSubscription = this._store
      .pipe(select(fromBookViewer.getSelectedBook))
      .subscribe(b => {
        if (b) {
          if (this._bookFormGroup) {
            this._bookFormGroup.reset();
          }

          this._bookFormGroup = this._formBuilder.group({
            title: ['', [Validators.required]],
            genre: ['', [Validators.required]],
            isbn: ['', [Validators.required]],
            authors: this._formBuilder.array(
              this.createAuthorGroups(b.authors.length)
            ),
            publisher: this._formBuilder.group({
              name: ['', [Validators.required]],
              contactPhone: ['', [Validators.required]],
              contactName: ['', [Validators.required]]
            }),
            publishDate: ['', [Validators.required]],
            pages: ['', [Validators.required]]
          });

          this._bookFormGroup.patchValue(b);
        }
      });
  }

  ngOnDestroy() {
    this._bookSubscription.unsubscribe();
    this._bookFormGroup.reset();
  }

  private createAuthorGroups(size: number): FormGroup[] {
    const result = [];

    for (let x = 0; x < size; x++) {
      result.push(
        this._formBuilder.group({
          firstName: ['', Validators.required],
          middleInitial: [''],
          lastName: ['', Validators.required]
        })
      );
    }
    return result;
  }
}
