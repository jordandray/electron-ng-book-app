import { Author } from './author';
import { Publisher } from './publisher';

export interface Book {
  id: number;
  title: string;
  genre: string;
  isbn: string;
  authors: Author[];
  publisher: Publisher;
  publishDate: string;
  pages: number;
}
