export interface Publisher {
  id: number;
  name: string;
  contactPhone?: string;
  contactName?: string;
}
