export interface Author {
  id: number;
  firstName: string;
  middleInitial?: string;
  lastName: string;
}
