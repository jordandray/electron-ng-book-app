import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { BookViewerRoutingModule } from './book-viewer-routing.module';
import { BookViewerComponent } from './components/book-viewer/book-viewer.component';
import { StoreModule } from '@ngrx/store';

import { reducer } from './state/reducers/book-viewer.reducer';
import { MaterialModule } from '../shared/material/material.module';
import { EffectsModule } from '@ngrx/effects';
import { BookViewerEffects } from './state/effects/book-viewer.effects';
import { BookDialogComponent } from './components/book-dialog/book-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    BookViewerComponent,
    BookDialogComponent
  ],
  imports: [
    CommonModule,
    BookViewerRoutingModule,
    AgGridModule.withComponents([]),
    MaterialModule,
    StoreModule.forFeature('bookViewer', reducer),
    EffectsModule.forFeature(
      [ BookViewerEffects ]
    ),
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    BookDialogComponent
  ]
})
export class BookViewerModule { }
