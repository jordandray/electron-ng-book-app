import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'book-viewer', loadChildren: './book-viewer/book-viewer.module#BookViewerModule' },
  { path: '', redirectTo: 'book-viewer', pathMatch: 'full' },
  { path: '**', redirectTo: 'book-viewer', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
