const { app, BrowserWindow } = require('electron');
const path = require('path');
const url = require('url');

let win;

function createWindow() {
  win = new BrowserWindow({
    width: 1024,
    height: 768
  });

  win.loadURL(
      url.format({
        pathname: path.join(__dirname, '/dist/index.html'),
        protocol: 'file:',
        slashes: true
      })
    );
  }
  app.on('ready', createWindow);

  app.on('activate', () => {
    if (win === null) {
      createWindow();
    }
  });

  app.on('window-all-closed', () => {
    // Closing windows does not quit apps on macOS
    if (process.platform !== 'darwin') {
      app.quit();
    }
  });
